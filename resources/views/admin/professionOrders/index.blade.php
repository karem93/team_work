@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 main-box">

            <div class="col-12 px-0">
                <div class="col-12 p-0 row">
                    <div class="col-12 col-lg-4 py-3 px-3">
                        <span class="fas fa-orders"></span> طلبات التوظيف
                    </div>
                    <div class="col-12 col-lg-4 p-0">
                    </div>
                    <div class="col-12 col-lg-4 p-2 text-lg-end">
                    </div>
                </div>
                <div class="col-12 divider" style="min-height: 2px;"></div>
            </div>

            <div class="col-12 py-2 px-2 row">
                <div class="col-12 col-lg-4 p-2">
                    <form method="GET">
                        <input type="text" name="q" class="form-control" placeholder="بحث ... "
                               value="{{request()->get('q')}}">
                    </form>
                </div>
            </div>
            <div class="col-12 p-3" style="overflow:auto">
                <div class="col-12 p-0" style="min-width:1100px;">


                    <table class="table table-bordered  table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الوظيفة</th>
                            <th>الاسم</th>
                            <th>الايميل</th>
                            <th>الجوال</th>
                            <th>العنوان</th>
                            <th>تحكم</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($profession_orders as $order))
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$order?->profession->title_ar}}</td>
                                <td>{{$order->name}}</td>
                                <td>{{$order->email}}</td>
                                <td>{{$order->phone}}</td>
                                <td>{{$order->city->name_ar}} / {{ $order->district }}</td>
                                <td style="width: 180px;">

                                    <a href="{{ asset($order->getFirstMediaUrl('cv')) }}" download="">
							        <span class="btn  btn-outline-success btn-sm font-1 mx-1">
								        <span class="fas fa-cloud-download"></span> CV
                                    </span>
                                    </a>


                                    <form method="POST" action="{{route('admin.profession_orders.destroy',$order)}}"
                                          class="d-inline-block">@csrf @method("DELETE")
                                        <button class="btn  btn-outline-danger btn-sm font-1 mx-1"
                                                onclick="var result = confirm('هل أنت متأكد من عملية الحذف ؟');if(result){}else{event.preventDefault()}">
                                            <span class="fas fa-trash "></span> حذف
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12 p-3">
                {{$profession_orders->appends(request()->query())->render()}}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="module">
        $('.change_slider_status').on('change', function () {
            $.ajax({
                url: "{{route('admin.profession_orders.change_status')}}",
                method: "POST",
                data: {_token: "{{csrf_token()}}", id: $(this).attr('data-id')}
            }).done(function (res) {
            });
        });
    </script>
@endpush
