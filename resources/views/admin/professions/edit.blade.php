@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.professions.update',$profession)}}">
                @csrf
                @method("PUT")
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-12 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> تعديل
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الاسم بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="title_ar" required maxlength="190" class="form-control"
                                       value="{{$profession->title_ar}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الاسم بالانلجيزية
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="title_en" required maxlength="190" class="form-control"
                                       value="{{$profession->title_en}}">
                            </div>
                        </div>


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الدولة
                            </div>
                            <div class="col-12 pt-3">
                                <select class="form-control select2-select" name="country_id" id="country-dropdown"
                                        required>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}"
                                                @if($profession->city->country_id == $country->id) selected @endif >{{$country->name_ar}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                المدينة
                            </div>
                            <div class="col-12 pt-3">
                                <select class="form-control select2-select" name="city_id" id="city_dropdown"
                                        required>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}"
                                                @if($profession->city_id == $city->id) selected @endif >{{$city->name_ar}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الخدمة
                            </div>
                            <div class="col-12 pt-3">
                                <select class="form-control select2-select" id="country-dropdown" name="service_id">
                                    <option disabled selected>اختر الخدمة</option>
                                    @foreach($services as $service)
                                        <option value="{{$service->id}}"
                                                @if($profession->service_id == $service->id) selected @endif>{{$service->title_ar}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الدوام
                            </div>

                            <div class="col-12 pt-3">
                                <select class="form-control select2-select" name="shift" required>
                                    <option value="full" @if($profession->shift == 'full') selected @endif >دوام كلي
                                    </option>
                                    <option value="part" @if($profession->shift == 'part') selected @endif>دوام جزئي
                                    </option>
                                    <option value="freelance" @if($profession->shift == 'freelance') selected @endif>عمل
                                        حر
                                    </option>
                                </select>
                            </div>
                        </div>


                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                وصف مختصر الوظيفة بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="meta_description_ar" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{ $profession->meta_description_ar }}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                وصف مختصر الوظيفة بالانجليزية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="meta_description_en" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{ $profession->meta_description_en }}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                وصف الوظيفة بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="description_ar" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{ $profession->description_ar }}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                وصف الوظيفة بالانجليزية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="description_en" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{ $profession->description_en }}</textarea>
                            </div>
                        </div>


                    </div>

                    <div class="col-12 p-3">
                        <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                    </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {

            /*------------------------------------------
            --------------------------------------------
            Country Dropdown Change Event
            --------------------------------------------
            --------------------------------------------*/
            $('#country-dropdown').on('change', function () {
                var idCountry = this.value;
                $("#city-dropdown").html('');
                $.ajax({
                    url: "{{ route('admin.getCities') }}",
                    type: "POST",
                    data: {
                        country_id: idCountry,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $('#city-dropdown').html('<option value="">-- اختر المدينة --</option>');
                        $.each(result.cities, function (key, value) {
                            $("#city_dropdown").append('<option value="' + value
                                .id + '">' + value.name_ar + '</option>');
                        });

                    }
                });
            });
        });
    </script>
@endsection
