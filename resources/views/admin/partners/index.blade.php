@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 main-box">

            <div class="col-12 px-0">
                <div class="col-12 p-0 row">
                    <div class="col-12 col-lg-4 py-3 px-3">
                        <span class="fas fa-partners"></span> الشركاء
                    </div>
                    <div class="col-12 col-lg-4 p-0">
                    </div>
                    <div class="col-12 col-lg-4 p-2 text-lg-end">

                        <a href="{{route('admin.partners.create')}}">
                            <span class="btn btn-primary"><span class="fas fa-plus"></span> إضافة جديد</span>
                        </a>
                    </div>
                </div>
                <div class="col-12 divider" style="min-height: 2px;"></div>
            </div>

            <div class="col-12 py-2 px-2 row">
                <div class="col-12 col-lg-4 p-2">
                    <form method="GET">
                        <input type="text" name="q" class="form-control" placeholder="بحث ... "
                               value="{{request()->get('q')}}">
                    </form>
                </div>
            </div>
            <div class="col-12 p-3" style="overflow:auto">
                <div class="col-12 p-0" style="min-width:1100px;">


                    <table class="table table-bordered  table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الصورة</th>
                            <th>العنوان</th>
                            <th style="width:100px;">تفعيل</th>
                            <th>تحكم</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($partners as $partner)
                            <tr>
                                <td>{{$partner->id}}</td>
                                <td><img src="{{$partner->image()}}" style="width:40px"></td>
                                <td>{{$partner->title}}</td>
                                <td>
                                    @if($partner)
                                        <div class="form-switch">
                                            <input name="id" class="form-check-input change_partner_status"
                                                   type="checkbox"
                                                   id="flexSwitchCheckDefault"
                                                   {{old('is_active',$partner??0)=="1"?"checked":""}} style="width: 50px;height:25px"
                                                   value="1" data-id="{{$partner->id}}">
                                        </div>
                                    @endif
                                </td>
                                <td style="width: 180px;">

                                        <a href="{{route('admin.partners.edit',$partner)}}">
							<span class="btn  btn-outline-success btn-sm font-1 mx-1">
								<span class="fas fa-wrench "></span> تحكم
							</span>
                                        </a>

                                        <form method="POST" action="{{route('admin.partners.destroy',$partner)}}"
                                              class="d-inline-block">@csrf @method("DELETE")
                                            <button class="btn  btn-outline-danger btn-sm font-1 mx-1"
                                                    onclick="var result = confirm('هل أنت متأكد من عملية الحذف ؟');if(result){}else{event.preventDefault()}">
                                                <span class="fas fa-trash "></span> حذف
                                            </button>
                                        </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12 p-3">
                {{$partners->appends(request()->query())->render()}}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="module">
        $('.change_partner_status').on('change', function () {
            $.ajax({
                url: "{{route('admin.partners.change_status')}}",
                method: "POST",
                data: {_token: "{{csrf_token()}}", id: $(this).attr('data-id')}
            }).done(function (res) {
            });
        });
    </script>
@endpush
