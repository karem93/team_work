@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.services.update',$service)}}">
                @csrf
                @method("PUT")
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-12 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> تعديل
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">
                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الاسم بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="title_ar" required maxlength="190" class="form-control"
                                       value="{{$service->title_ar}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الاسم بالانجليزية
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="title_en" required maxlength="190" class="form-control"
                                       value="{{$service->title_en}}">
                            </div>
                        </div>


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                وصف الخدمة بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="meta_description_ar" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{$service->meta_description_ar}}</textarea>
                            </div>
                        </div>


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                وصف الخدمة بالانجليزية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="meta_description_en" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{$service->meta_description_en}}</textarea>
                            </div>
                        </div>


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الوصف بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="description_ar" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{$service->description_ar}}</textarea>
                            </div>
                        </div>


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الوصف بالانجليزية
                            </div>
                            <div class="col-12 pt-3">
                                <textarea name="description_en" class="editor with-file-explorer" id="" cols="30"
                                          rows="10">{{$service->description_en}}</textarea>
                            </div>
                        </div>


                        <div class="col-12 p-2">
                            <div class="col-12">
                                الصورة
                            </div>
                            <div class="col-12 pt-3">
                                <input type="file" name="image" class="form-control" accept="image/*">
                            </div>
                            <div class="col-12 pt-3">
                                <img src="{{$service->image()}}" style="width:100px">
                            </div>
                        </div>

                    </div>

                    <div class="col-12 p-3">
                        <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
