@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.cities.update',$city)}}">
                @csrf
                @method("PUT")
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-12 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> تعديل
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الاسم بالعربية
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="name_ar" required maxlength="190" class="form-control"
                                       value="{{$city->name_ar}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الاسم بالانلجيزية
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="name_en" required maxlength="190" class="form-control"
                                       value="{{$city->name_en}}">
                            </div>
                        </div>


                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الدولة
                            </div>
                            <div class="col-12 pt-3">
                                <select class="form-control select2-select" name="country_id" required>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($city->country_id == $country->id) selected @endif >{{$country->name_ar}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="col-12 p-3">
                        <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
