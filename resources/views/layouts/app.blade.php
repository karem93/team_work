<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}"
      lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content="...."/>
    <meta name="author" content="misara adel"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
        شركة تيم ورك - الرئيسية
    </title>
    <link rel="shortcut icon" href="{{ asset('site/assets/images/logo/icon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/swiper-bundle.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/jquery.fancybox.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/style.css') }}"/>
</head>
<body>

@yield('content')


<script src="{{ asset('site/assets/js/lib/jquery4.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/popper.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/bootstrap.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/jquery.fancybox.js') }}"></script>
<script src="{{ asset('site/assets/js/main.js') }}"></script>


@yield('custom_scripts')


</body>
</html>
