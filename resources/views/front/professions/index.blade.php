<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}"
      lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content="...."/>
    <meta name="author" content="misara adel"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>
        شركة تيم ورك - الرئيسية
    </title>
    <link rel="shortcut icon" href="{{ asset('site/assets/images/logo/icon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/swiper-bundle.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/jquery.fancybox.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/style.css') }}"/>
</head>
<body>

<nav class="navbar">
    <div class="container">
        <div class="contain">
            <div class="hamburger">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>

            <a href="/" class="brand-name">
                <img
                    src="{{ asset('site/assets/images/logo/logo.svg') }}"
                    loading="lazy"
                    alt=""/>
            </a>

            <div class="nav-contain">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="/" class="nav-link {{ request()->is('home') ? 'active' : '' }}">
                            {{ __('lang.home') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#about_us"
                           class="nav-link" {{ str_contains(url()->current(), 'about_us') ? 'active' : '' }}>
                            {{ __('lang.about') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#why_teamwork"
                           class="nav-link" {{ str_contains(url()->current(), 'why_teamwork') ? 'active' : '' }}>
                            {{ __('lang.why_team')}}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('home.list_services') }}"
                           class="nav-link" {{ str_contains(url()->current(), 'services') ? 'active' : '' }}>
                            {{ __('lang.services')}}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#sectors"
                           class="nav-link" {{ str_contains(url()->current(), 'sectors') ? 'active' : '' }}>
                            {{ __('lang.sections') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#partners"
                           class="nav-link" {{ str_contains(url()->current(), 'partners') ? 'active' : '' }}>
                            {{ __('lang.partners') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('home.list_professions') }}"
                           class="nav-link {{ str_contains(url()->current(), 'professions') ? 'active' : '' }}">
                            {{ __('lang.professions') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('home.show_contact') }}"
                           class="nav-link {{ str_contains(url()->current(), 'contact_us') ? 'active' : '' }}">
                            {{ __('lang.contact_us') }}
                        </a>
                    </li>
                </ul>

                <div class="button-contain">
                    <a href="#" class="lang">
                <span>
                   @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode != LaravelLocalization::getCurrentLocale())
                            <a style="color: #fff" rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                        </a>
                        @endif
                    @endforeach
                </span>

                        <img
                            src="{{ asset('site/assets/images/icons/global.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>

                    <a href="#" class="custom-btn secondary-btn">
                <span>
                  {{ __('lang.request_service') }}
                </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>


<main>
    <section class="sub-header large-sub-header">
        <div class="container">
            <div class="header-contain">
                <h1>
                    {{ __('lang.professions') }}
                </h1>

                <p>
                    {{ __('lang.join_us_desc') }}
                </p>
            </div>
        </div>
    </section>

    <section class="jobs general-section">
        <div class="container">
            <div class="row">

                @forelse($professions as $profession)
                    <div class="col-12 mb-3">
                        <div class="job-box">
                            <div class="flex-contain">
                                <div class="data">
                                    <ul class="tags">
                                        <li>
                                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $profession->service->title_ar : $profession->service->title_en }}
                                        </li>

                                        <li>
                                            {{ __('lang.'.$profession->shift) }}
                                        </li>
                                    </ul>

                                    <h1>
                                        {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $profession?->title_ar : $profession?->title_en }}
                                    </h1>

                                    <ul class="loactions">
                                        <li>
                                            <img
                                                src="{{ asset('site/assets/images/icons/location.svg') }}"
                                                loading="lazy"
                                                alt=""/>

                                            <span>
                             {!! LaravelLocalization::getCurrentLocale() == 'ar' ? $profession->city?->country?->name_ar . ' , ' . $profession?->city?->name_ar : $profession?->city?->country?->name_en . ' , ' . $profession?->city?->name_en !!}

                        </span>
                                        </li>

                                        <li>
                                            <img
                                                src="{{ asset('site/assets/images/icons/clock.svg') }}"
                                                loading="lazy"
                                                alt=""/>

                                            <span>
                          {{ $profession->created_at->diffForHumans() }}
                        </span>
                                        </li>
                                    </ul>
                                </div>

                                <a href="{{ route('home.show_profession', $profession->id) }}"
                                   class="custom-btn secondary-btn">
                    <span>
                      {{ __('lang.apply_job') }}
                    </span>
                                </a>
                            </div>

                            <p>
                                {!!
                                LaravelLocalization::getCurrentLocale() == 'ar' ? $profession?->meta_description_ar : $profession?->meta_description_en
                         !!}
                            </p>
                        </div>
                    </div>
                @empty
                    <div class="col-12 text-center">
                        <p>
                            {{ __('lang.no_professions') }}
                        </p>
                    </div>
                @endforelse

                <div class="col-12">
                    <ul class="pagination">
                        {{$professions->appends(request()->query())->render()}}
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>


<div class="modal fade" id="DoneModal" tabindex="-1" role="dialog" aria-labelledby="DoneModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="contain">
                    <img src="{{ asset('site/assets/images/icons/done.svg') }}" loading="lazy" alt=""/>
                    <!-- <img src="assets/images/icons/error.svg" loading="lazy" alt="" /> -->
                    <h1>
                        {{ __('lang.send_success') }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 mb-3">
                <div class="contain">
                    <a href="/" class="brand-name">
                        <img
                            src="{{ asset('site/assets/images/logo/logo.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>

                    <p>
                        {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['website_bio'] : $settings['website_bio_en']}}
                    </p>

                    <ul class="socail">
                        <li>
                            <a href="{{ $settings['facebook_link'] }}" target="_blank">
                                <img
                                    src="{{ asset('site/assets/images/footer/facebook.svg') }}"
                                    loading="lazy"
                                    alt=""/>
                            </a>
                        </li>

                        <li>
                            <a href="{{ $settings['instagram_link'] }}" target="_blank">
                                <img
                                    src="{{ asset('site/assets/images/footer/instagram.svg') }}"
                                    loading="lazy"
                                    alt=""/>
                            </a>
                        </li>

                        <li>
                            <a href="{{ $settings['twitter_link'] }}" target="_blank">
                                <img
                                    src="{{ asset('site/assets/images/footer/x.svg') }}"
                                    loading="lazy"
                                    alt=""/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-12 mb-3">
                <div class="contain padding-data">
                    <h1>
                        {{ __('lang.site_sections') }}
                    </h1>

                    <ul class="links">
                        <li>
                            <a href="/">
                                {{ __('lang.home') }}
                            </a>
                        </li>

                        <li>
                            <a href="/#about_us">
                                {{  __('lang.about') }}
                            </a>
                        </li>

                        <li>
                            <a href="/#why_teamwork">
                                {{  __('lang.why_team') }}
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('home.list_services') }}">
                                {{ __('lang.services') }}
                            </a>
                        </li>

                        <li>
                            <a href="/#sectors">
                                {{ __('lang.sections') }}
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('home.list_professions') }}">
                                {{ __('lang.professions') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-12 mb-3">
                <div class="contain">
                    <h1>
                        {{ __('lang.contact_us') }}
                    </h1>

                    <a href="tel:{{ $settings['phone'] }}" target="_parent" class="contact-data">
                        <div class="data">
<span>
{{ __('lang.contact_us_now') }}
</span>
                            <p>
                                {{ $settings['phone'] }}
                            </p>
                        </div>

                        <img
                            src="{{ asset('site/assets/images/footer/call.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>

                    <a href="mailto:Tw@gmail.com" target="_parent" class="contact-data">
                        <div class="data">
<span>
{{ __('lang.contact_us') }}
</span>

                            <p>
                                {{ $settings['contact_email'] }}
                            </p>
                        </div>

                        <img
                            src="{{ asset('site/assets/images/footer/sms.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>
                </div>
            </div>
        </div>

        <div class="copyrights">
            <p>
                {{ __('lang.rights') }}
            </p>
        </div>
    </div>
</footer>

<div class="fixed-icon">
    <a href="tel:{{ $settings['phone'] }}" target="_blank" class="icon primary-color">
        <img
            src="{{ asset('site/assets/images/footer/call.svg') }}"
            loading="lazy"
            alt=""/>
    </a>

    <a href="https://wa.me/{{  $settings['whatsapp_phone']}}" target="_blank" class="icon secondary-color">
        <img
            src="{{ asset('site/assets/images/footer/whats.svg') }}"
            loading="lazy"
            alt=""/>
    </a>
</div>


<script src="{{ asset('site/assets/js/lib/jquery4.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/popper.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/bootstrap.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/jquery.fancybox.js') }}"></script>
<script src="{{ asset('site/assets/js/main.js') }}"></script>

</body>
</html>
