<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}"
      lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content="...."/>
    <meta name="author" content="misara adel"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
        شركة تيم ورك - الرئيسية
    </title>
    <link rel="shortcut icon" href="{{ asset('site/assets/images/logo/icon.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/swiper-bundle.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/lib/jquery.fancybox.css') }}"/>
    <link rel="stylesheet" href="{{ asset('site/assets/css/style.css') }}"/>
</head>
<body>

<nav class="navbar">
    <div class="container">
        <div class="contain">
            <div class="hamburger">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>

            <a href="/" class="brand-name">
                <img
                    src="{{ asset('site/assets/images/logo/logo.svg') }}"
                    loading="lazy"
                    alt=""/>
            </a>

            <div class="nav-contain">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="/" class="nav-link {{ request()->is('home') ? 'active' : '' }}">
                            {{ __('lang.home') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#about_us"
                           class="nav-link" {{ str_contains(url()->current(), 'about_us') ? 'active' : '' }}>
                            {{ __('lang.about') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#why_teamwork"
                           class="nav-link" {{ str_contains(url()->current(), 'why_teamwork') ? 'active' : '' }}>
                            {{ __('lang.why_team')}}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('home.list_services') }}"
                           class="nav-link" {{ str_contains(url()->current(), 'services') ? 'active' : '' }}>
                            {{ __('lang.services')}}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/#sectors"
                           class="nav-link" {{ str_contains(url()->current(), 'sectors') ? 'active' : '' }}>
                            {{ __('lang.sections') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#partners"
                           class="nav-link" {{ str_contains(url()->current(), 'partners') ? 'active' : '' }}>
                            {{ __('lang.partners') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('home.list_professions') }}"
                           class="nav-link {{ str_contains(url()->current(), 'professions') ? 'active' : '' }}">
                            {{ __('lang.professions') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('home.show_contact') }}"
                           class="nav-link {{ str_contains(url()->current(), 'contact_us') ? 'active' : '' }}">
                            {{ __('lang.contact_us') }}
                        </a>
                    </li>
                </ul>

                <div class="button-contain">
                    <a href="#" class="lang">
                <span>
                   @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode != LaravelLocalization::getCurrentLocale())
                            <a style="color: #fff" rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                        </a>
                        @endif
                    @endforeach
                </span>

                        <img
                            src="{{ asset('site/assets/images/icons/global.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>

                    <a href="#" class="custom-btn secondary-btn">
                <span>
                  {{ __('lang.request_service') }}
                </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>



<main>
    <section class="sub-header large-sub-header">
        <div class="container">
        </div>
    </section>

    <section class="job-details general-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-12 mx-auto mb-4">
                    <div class="job-data">
                        <ul class="tags">
                            @if($profession->service)
                                <li>
                                    {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $profession->service->title_ar : $profession->service->title_en }}
                                </li>
                            @endif

                            <li>
                                {{ __('lang.'.$profession->shift) }}
                            </li>
                        </ul>

                        <h1>
                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $profession->title_ar : $profession->title_en }}
                        </h1>

                        <ul class="loactions">
                            <li>
                                <img
                                    src="{{ asset('site/assets/images/icons/location.svg') }}"
                                    loading="lazy"
                                    alt=""/>

                                <span>
                                        {!! LaravelLocalization::getCurrentLocale() == 'ar' ? $profession->city->country->name_ar . ' , ' . $profession->city->name_ar : $profession->city->country->name_en . ' , ' . $profession->city->name_en !!}
                    </span>
                            </li>

                            <li>
                                <img
                                    src="{{ asset('site/assets/images/icons/clock.svg') }}"
                                    loading="lazy"
                                    alt=""/>

                                <span>
                       {{ $profession->created_at->diffForHumans() }}
                    </span>
                            </li>
                        </ul>
                        <h2>
                            {{ __('lang.job_desc')}}
                        </h2>

                        <ul class="desc-list">
                            {!! LaravelLocalization::getCurrentLocale() == 'ar' ? $profession->description_ar : $profession->description_en !!}
                        </ul>
                    </div>
                </div>

                <div class="col-lg-10 col-12 mx-auto mt-3">
                    <form class="form-contain" id="profession_form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="profession_id" value="{{ $profession->id }}">
                        <h1 class="title">
                            {{ __('lang.apply_job') }}
                        </h1>

                        <h2 class="sub-title">
                            {{ __('lang.main_data') }}
                        </h2>

                        <div class="data-contain">
                            <div class="form-data">
                                <label for="">
                                    {{ __('lang.full_name') }}
                                </label>
                                <div class="form-group">
                                    <input
                                        type="text"
                                        required
                                        name="name"
                                        class="form-control"
                                        placeholder="{{ __('lang.enter_full_name') }}"/>
                                </div>
                            </div>

                            <div class="form-data">
                                <label for="">
                                    {{ __('lang.email') }}
                                </label>
                                <div class="form-group">
                                    <input
                                        type="email"
                                        name="email"
                                        required
                                        class="form-control"
                                        placeholder="{{ __('lang.enter_email') }}"/>
                                </div>
                            </div>

                            <div class="form-data">
                                <label for="">
                                    {{ __('lang.phone') }}
                                </label>
                                <div class="form-group">
                                    <input
                                        type="text"
                                        name="phone"
                                        class="form-control"
                                        placeholder="{{ __('lang.enter_phone') }}"/>
                                </div>

                            </div>
                        </div>

                        <h2 class="sub-title">
                            {{ __('lang.address') }}
                        </h2>

                        <div class="data-contain">
                            <div class="form-data">
                                <label for="city_id">
                                    {{ __('lang.city') }}
                                </label>
                                <div class="form-group">
                                    <select name="city_id" class="form-control">
                                        <option disabled selected>{{ __('lang.enter_city') }}</option>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}">
                                                {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $city->name_ar : $city->name_en }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-data">
                                <label for="">
                                    {{ __('lang.district') }}
                                </label>
                                <div class="form-group">
                                    <input
                                        type="text"
                                        name="district"
                                        class="form-control"
                                        placeholder="{{ __('lang.enter_district') }}"/>
                                </div>

                            </div>
                        </div>

                        <h2 class="sub-title">
                            {{ __('lang.skills') }}
                        </h2>

                        <div class="data-contain">
                            <div class="form-data">
                                <label for="">
                                    {{ __('lang.skills') }}
                                </label>

                                <div class="form-group text-area">
                                        <textarea name="skills" class="form-control" placeholder="..." id="" cols="30"
                                                  rows="10"></textarea>
                                </div>
                            </div>

                            <div class="form-data">
                                <label for="">
                                    {{ __('lang.cv') }}
                                </label>

                                <div class="form-group file-input-group">
                                    <div class="file__input" id="file__input_worker">
                                        <input type="file" id="worker_resume" name="cv"
                                               class="form-control file__input--file_worker"
                                               accept=".pdf, .doc, .docx"/>
                                        <label for="worker_resume" class="file__input--label form-label-shape">
                                            <img src="{{ asset('site/assets/images/icons/file.svg') }}"
                                                 loading="lazy" alt=""/>
                                            {{ __('lang.upload_cv') }}
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="validation-errors"></div>
                        <div id="result"></div>

                        <button type="submit" id="submitBtn" onclick="submitForm()"
                                class="custom-btn secondary-btn">
                            <span>{{ __('lang.send') }}</span>
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </section>
</main>


<div class="modal fade" id="DoneModal" tabindex="-1" role="dialog" aria-labelledby="DoneModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="contain">
                    <img src="{{ asset('site/assets/images/icons/done.svg') }}" loading="lazy" alt=""/>
                    <!-- <img src="assets/images/icons/error.svg" loading="lazy" alt="" /> -->
                    <h1>
                        {{ __('lang.send_success') }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>



<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 mb-3">
                <div class="contain">
                    <a href="/" class="brand-name">
                        <img
                            src="{{ asset('site/assets/images/logo/logo.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>

                    <p>
                        {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['website_bio'] : $settings['website_bio_en']}}
                    </p>

                    <ul class="socail">
                        <li>
                            <a href="{{ $settings['facebook_link'] }}" target="_blank">
                                <img
                                    src="{{ asset('site/assets/images/footer/facebook.svg') }}"
                                    loading="lazy"
                                    alt=""/>
                            </a>
                        </li>

                        <li>
                            <a href="{{ $settings['instagram_link'] }}" target="_blank">
                                <img
                                    src="{{ asset('site/assets/images/footer/instagram.svg') }}"
                                    loading="lazy"
                                    alt=""/>
                            </a>
                        </li>

                        <li>
                            <a href="{{ $settings['twitter_link'] }}" target="_blank">
                                <img
                                    src="{{ asset('site/assets/images/footer/x.svg') }}"
                                    loading="lazy"
                                    alt=""/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-12 mb-3">
                <div class="contain padding-data">
                    <h1>
                        {{ __('lang.site_sections') }}
                    </h1>

                    <ul class="links">
                        <li>
                            <a href="/">
                                {{ __('lang.home') }}
                            </a>
                        </li>

                        <li>
                            <a href="/#about_us">
                                {{  __('lang.about') }}
                            </a>
                        </li>

                        <li>
                            <a href="/#why_teamwork">
                                {{  __('lang.why_team') }}
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('home.list_services') }}">
                                {{ __('lang.services') }}
                            </a>
                        </li>

                        <li>
                            <a href="/#sectors">
                                {{ __('lang.sections') }}
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('home.list_professions') }}">
                                {{ __('lang.professions') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-12 mb-3">
                <div class="contain">
                    <h1>
                        {{ __('lang.contact_us') }}
                    </h1>

                    <a href="tel:{{ $settings['phone'] }}" target="_parent" class="contact-data">
                        <div class="data">
                                <span>
                                {{ __('lang.contact_us_now') }}
                                </span>
                            <p>
                                {{ $settings['phone'] }}
                            </p>
                        </div>

                        <img
                            src="{{ asset('site/assets/images/footer/call.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>

                    <a href="mailto:Tw@gmail.com" target="_parent" class="contact-data">
                        <div class="data">
                                <span>
                                {{ __('lang.contact_us') }}
                                </span>

                            <p>
                                {{ $settings['contact_email'] }}
                            </p>
                        </div>

                        <img
                            src="{{ asset('site/assets/images/footer/sms.svg') }}"
                            loading="lazy"
                            alt=""/>
                    </a>
                </div>
            </div>
        </div>

        <div class="copyrights">
            <p>
                {{ __('lang.rights') }}
            </p>
        </div>
    </div>
</footer>

<div class="fixed-icon">
    <a href="tel:{{ $settings['phone'] }}" target="_blank" class="icon primary-color">
        <img
            src="{{ asset('site/assets/images/footer/call.svg') }}"
            loading="lazy"
            alt=""/>
    </a>

    <a href="https://wa.me/{{  $settings['whatsapp_phone']}}" target="_blank" class="icon secondary-color">
        <img
            src="{{ asset('site/assets/images/footer/whats.svg') }}"
            loading="lazy"
            alt=""/>
    </a>
</div>


<script src="{{ asset('site/assets/js/lib/jquery4.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/popper.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/bootstrap.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('site/assets/js/lib/jquery.fancybox.js') }}"></script>
<script src="{{ asset('site/assets/js/main.js') }}"></script>


<script>
    $(document).ready(function () {
        $('#submitBtn').click(function (e) {
            e.preventDefault();
            var formData = new FormData($('#profession_form')[0]);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{!! route("home.store_profession_order")!!}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $("#DoneModal").modal('show')
                    $("#validation-errors").hide().css("display", "none");
                    $('#profession_form')[0].reset();
                },
                error: function (xhr) {
                    $('#validation-errors').html('');
                    $.each(xhr.responseJSON.errors, function (key, value) {
                        $('#validation-errors').append('<div class="alert alert-danger">' + value + '</div');
                    });
                },
            });
        });
    });
</script>


</body>
</html>



