@extends('layouts.app')
@section('content')

    <nav class="navbar">
        <div class="container">
            <div class="contain">
                <div class="hamburger">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>

                <a href="/" class="brand-name">
                    <img
                        src="{{ asset('site/assets/images/logo/logo.svg') }}"
                        loading="lazy"
                        alt=""/>
                </a>

                <div class="nav-contain">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="/" class="nav-link active">
                                {{ __('lang.home') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#about_us" class="nav-link" {{ str_contains(url()->current(), 'about_us') ? 'active' : '' }}>
                                {{ __('lang.about') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#why_teamwork" class="nav-link" {{ str_contains(url()->current(), 'why_teamwork') ? 'active' : '' }}>
                                {{ __('lang.why_team')}}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('home.list_services') }}" class="nav-link" {{ str_contains(url()->current(), 'services') ? 'active' : '' }}>
                                {{ __('lang.services')}}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#sectors" class="nav-link" {{ str_contains(url()->current(), 'sectors') ? 'active' : '' }}>
                                {{ __('lang.sections') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#partners" class="nav-link" {{ str_contains(url()->current(), 'partners') ? 'active' : '' }}>
                                {{ __('lang.partners') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('home.list_professions') }}" class="nav-link {{ str_contains(url()->current(), 'professions') ? 'active' : '' }}">
                                {{ __('lang.professions') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('home.show_contact') }}" class="nav-link {{ str_contains(url()->current(), 'contact_us') ? 'active' : '' }}">
                                {{ __('lang.contact_us') }}
                            </a>
                        </li>
                    </ul>

                    <div class="button-contain">
                        <a href="#" class="lang">
                <span>
                   @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                        @if($localeCode != LaravelLocalization::getCurrentLocale())
                            <a style="color: #fff" rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ mb_strtoupper(substr($properties['name'], 0, 2)) }}
                        </a>
                        @endif
                    @endforeach
                </span>

                            <img
                                src="{{ asset('site/assets/images/icons/global.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>

                        <a href="#" class="custom-btn secondary-btn">
                <span>
                  {{ __('lang.request_service') }}
                </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <main>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-12 mb-4">
                        <div class="contain">
                            <p class="title">
                                {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['website_name'] : $settings['website_name_en']}}
                            </p>

                            <h1>
                                {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['address'] : $settings['address_en']}}
                            </h1>

                            <p>
                                {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['website_bio'] : $settings['website_bio_en']}}
                            </p>

                            <div class="button-contain">
                                <a href="/#about_us" class="custom-btn secondary-btn">
                    <span>
                      {{ __('lang.about') }}
                    </span>
                                </a>

                                <a href="{{ route('home.show_contact') }}" class="custom-btn semi-primary">
                    <span>
                      {{ __('lang.contact_us') }}
                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <img
                src="{{ asset('site/assets/images/intro/intro.webp') }}"
                class="intro-img"
                loading="lazy"
                alt=""/>
        </header>

        <section class="about-us about-pattern general-section" id="about_us">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-12 mb-3">
                        <div class="image-contain">
                            <img
                                src="{{ asset('site/assets/images/about/about.webp') }}"
                                loading="lazy"
                                alt=""/>
                        </div>
                    </div>

                    <div class="col-lg-6 col-12 mb-3">
                        <div class="contain padding-data">
                            <h1>
                                {{ __('lang.what_we_do') }}
                            </h1>

                            <p>
                                {{ __('lang.what_we_do_desc') }}
                            </p>

                            <a href="tel:{{$settings['phone']}}" target="_blank" class="contact-icon">
                                <div class="icon-contain">
                    <span class="number">
                      {{ $services->count() }}
                    </span>

                                    <span class="word">
                      {{ __('lang.services')}}
                    </span>
                                </div>

                                <div class="data">
                                    <h2>
                                        {{ __('lang.contact_us') }}
                                    </h2>

                                    <p>
                                        {{$settings['phone']}}
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="message-contain">
                    <div class="row">
                        <div class="col-lg-5 col-12 mb-3">
                            <div class="box">
                                <img
                                    src="{{ asset('site/assets/images/about/message.svg') }}"
                                    loading="lazy"
                                    alt=""/>

                                <div class="data">
                                    <h2>
                                        {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $message_page->title : $message_page->title_en }}
                                    </h2>
                                    <p>
                                        {!! LaravelLocalization::getCurrentLocale() == 'ar' ? $message_page->description : $message_page->description_en !!}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-7 col-12 mb-3">
                            <div class="box flex-data">
                                <img
                                    src="{{ asset('site/assets/images/about/vision.svg') }}"
                                    loading="lazy"
                                    alt=""/>

                                <div class="data">
                                    <h2>
                                        {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $vision_page->title : $vision_page->title_en }}
                                    </h2>

                                    <p>
                                        {!! LaravelLocalization::getCurrentLocale() == 'ar' ? $vision_page->description : $vision_page->description_en !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="why general-section" id="why_teamwork">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-12 mb-3">
                        <div class="contain">
                            <h1>
                                {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $why_us_page->title : $why_us_page->title_en }}
                            </h1>

                            <p>
                                {!!   LaravelLocalization::getCurrentLocale() == 'ar' ? $why_us_page->description : $why_us_page->description_en !!}
                            </p>

                            <ul class="list">
                                <li>
                                    {{ __('lang.why_1') }}
                                </li>

                                <li>
                                    {{ __('lang.why_2') }}
                                </li>

                                <li>
                                    {{ __('lang.why_3') }}
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-8 col-12 mb-3">
                        <div class="row">
                            <div class="col-lg-6 col-6 mb-3 box-contain">
                                <div class="box">
                                    <img
                                        src="{{ asset('site/assets/images/why/why_1.svg') }}"
                                        loading="lazy"
                                        alt=""/>

                                    <h2>
                                        {{ __('lang.flexible_contracts') }}
                                    </h2>

                                    <p>
                                        {{ __('lang.flexible_contracts_desc') }}
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-6 mb-3 box-contain">
                                <div class="box">
                                    <img
                                        src="{{ asset('site/assets/images/why/why_2.svg') }}"
                                        loading="lazy"
                                        alt=""/>

                                    <h2>
                                        {{ __('lang.competitive_prices') }}
                                    </h2>

                                    <p>
                                        {{ __('lang.competitive_prices_desc') }}
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-6 mb-3 box-contain">
                                <div class="box">
                                    <img
                                        src="{{ asset('site/assets/images/why/why_3.svg') }}"
                                        loading="lazy"
                                        alt=""/>

                                    <h2>
                                        {{ __('lang.innovative_services') }}
                                    </h2>

                                    <p>
                                        {{ __('lang.innovative_services_desc') }}
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-6 mb-3 box-contain">
                                <div class="box">
                                    <img
                                        src="{{ asset('site/assets/images/why/why_4.svg') }}"
                                        loading="lazy"
                                        alt=""/>

                                    <h2>
                                        {{ __('lang.take responsibility') }}
                                    </h2>

                                    <p>
                                        {{ __('lang.take_responsibility_desc') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="services general-section" id="services">
            <div class="container">
                <div class="heading">
                    <h1>
                        {{ __('lang.teamwork_services') }}
                    </h1>

                    <p>
                        {{ __('lang.teamwork_services_desc') }}
                    </p>
                </div>

                <div class="swiper swiper-services">
                    <div class="swiper-wrapper">
                        @foreach($services as $service)
                            <div class="swiper-slide">
                                <div class="service-box">
                                    <img
                                        src="{{ $service->image() }}"
                                        loading="lazy"
                                        class="service-img"
                                        alt=""/>

                                    <div class="contain">
                                        <h2>
                                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $service->title_ar : $service->title_en }}
                                        </h2>

                                        <p>
                                            {!!   LaravelLocalization::getCurrentLocale() == 'ar' ? $service->meta_description_ar : $service->meta_description_en !!}
                                        </p>

                                        <a href="{{ route('home.show_service',$service->id) }}"
                                           class="custom-btn secondary-btn">
                                            <span>
                                            {{ __('lang.service_details') }}
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </section>

        <section class="all-sections general-section" id="sectors">
            <div class="container">
                <div class="heading">
                    <h1>
                        {{ __('lang.target_sectors') }}
                    </h1>

                    <p>
                        {{ __('lang.target_sectors_desc') }}
                    </p>
                </div>

                <div class="row">
                    <div class="col-lg-8 col-12 mx-auto">
                        <div class="row">
                            @foreach($categories as $category)
                                <div class="col-lg-4 col-6 mb-3 mx-auto">
                                    <div class="box">
                                        <img
                                            src="{{ $category->image() }}"
                                            loading="lazy"
                                            class="box-icon"
                                            alt=""/>

                                        <p>
                                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $category->title_ar : $category->title_en }}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <img
                src="{{ asset('site/assets/images/all_sections/all_sections.webp') }}"
                loading="lazy"
                class="all-section-img"
                alt=""/>
        </section>

        <section class="contact-us general-section" id="ask_service">
            <div class="container">
                <div class="contact-contain">
                    <div class="row">
                        <div class="col-lg-6 col-12 mb-3">
                            <div class="contain">
                                <h1>
                                    {{ __('lang.ask_service') }}
                                </h1>

                                <p>
                                    {{ __('lang.ask_service_desc') }}
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-6 col-12 mb-3">
                            <form class="form-contain" id="service_form">
                                @csrf

                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <div class="form-data">
                                            <div class="form-group">
                                                <input
                                                    required
                                                    type="text"
                                                    name="name"
                                                    class="form-control"
                                                    placeholder="{{ __('lang.name') }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-12">
                                        <div class="form-data">
                                            <div class="form-group">
                                                <input
                                                    required
                                                    type="text"
                                                    name="phone"
                                                    class="form-control"
                                                    placeholder="{{ __('lang.phone') }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-12">
                                        <div class="form-data">
                                            <div class="form-group">
                                                <input
                                                    required
                                                    type="email"
                                                    name="email"
                                                    class="form-control"
                                                    placeholder="{{ __('lang.email') }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-12">
                                        <div class="form-data">
                                            <div class="form-group select-data">
                                                <select name="service_id" class="form-control" id="" required>
                                                    <option selected disabled>{{__('lang.select_service')}}</option>
                                                    @foreach($services as $service)
                                                        <option value="{{ $service->id }}">
                                                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $service->title_ar : $service->title_en }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-data">
                                            <div class="form-group text-area">
                                            <textarea
                                                required
                                                name="message"
                                                class="form-control"
                                                placeholder="{{ __('lang.message') }}"
                                                id="" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="validation-errors"></div>

                                <button type="submit" id="submitBtn" class="custom-btn secondary-btn">
                                    <span>{{ __('lang.send') }}</span>
                                </button>
                            </form>
                        </div>
                    </div>

                    <img
                        src="{{ asset('site/assets/images/contact/contact_pattern.webp') }}"
                        class="contact-img"
                        loading="lazy"
                        alt=""/>
                </div>
            </div>
        </section>

        <section id="partners" class="partners general-section">
            <div class="container">
                <div class="heading">
                    <h1>
                        {{ __('lang.success_partners')}}
                    </h1>

                    <p>
                        {{ __('lang.success_partners_desc')}}
                    </p>
                </div>

                <div class="swiper swiper-partners">
                    <div class="swiper-wrapper">

                        @foreach($partners as $partner)
                            <div class="swiper-slide">
                                <div class="partner-box">
                                    <img
                                        src="{{ $partner->image() }}"
                                        loading="lazy"
                                        alt=""/>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </section>

        <section class="join-us">
            <div class="container">
                <div class="contain">
                    <p>
                        {{ __('lang.join_us') }}
                    </p>

                    <h1>
                        {{ __('lang.join_us_desc') }}
                    </h1>

                    <a href="{{ route('home.list_professions') }}" class="custom-btn primay-btn">
                        <span>
                        {{ __('lang.join_us_now') }}
                        </span>
                    </a>
                </div>
            </div>
        </section>
    </main>

    <div class="modal fade" id="DoneModal" tabindex="-1" role="dialog" aria-labelledby="DoneModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="contain">
                        <img src="{{ asset('site/assets/images/icons/done.svg') }}" loading="lazy" alt=""/>
                        <!-- <img src="assets/images/icons/error.svg" loading="lazy" alt="" /> -->
                        <h1>
                            {{ __('lang.send_success') }}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12 mb-3">
                    <div class="contain">
                        <a href="/" class="brand-name">
                            <img
                                src="{{ asset('site/assets/images/logo/logo.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>

                        <p>
                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['website_bio'] : $settings['website_bio_en']}}
                        </p>

                        <ul class="socail">
                            <li>
                                <a href="{{ $settings['facebook_link'] }}" target="_blank">
                                    <img
                                        src="{{ asset('site/assets/images/footer/facebook.svg') }}"
                                        loading="lazy"
                                        alt=""/>
                                </a>
                            </li>

                            <li>
                                <a href="{{ $settings['instagram_link'] }}" target="_blank">
                                    <img
                                        src="{{ asset('site/assets/images/footer/instagram.svg') }}"
                                        loading="lazy"
                                        alt=""/>
                                </a>
                            </li>

                            <li>
                                <a href="{{ $settings['twitter_link'] }}" target="_blank">
                                    <img
                                        src="{{ asset('site/assets/images/footer/x.svg') }}"
                                        loading="lazy"
                                        alt=""/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mb-3">
                    <div class="contain padding-data">
                        <h1>
                            {{ __('lang.site_sections') }}
                        </h1>

                        <ul class="links">
                            <li>
                                <a href="#">
                                    {{ __('lang.home') }}
                                </a>
                            </li>

                            <li>
                                <a href="#about_us">
                                    {{  __('lang.about') }}
                                </a>
                            </li>

                            <li>
                                <a href="#why_teamwork">
                                    {{  __('lang.why_team') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('home.list_services') }}">
                                    {{ __('lang.services') }}
                                </a>
                            </li>

                            <li>
                                <a href="#sectors">
                                    {{ __('lang.sections') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('home.list_professions') }}">
                                    {{ __('lang.professions') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mb-3">
                    <div class="contain">
                        <h1>
                            {{ __('lang.contact_us') }}
                        </h1>

                        <a href="tel:{{ $settings['phone'] }}" target="_parent" class="contact-data">
                            <div class="data">
                                <span>
                                {{ __('lang.contact_us_now') }}
                                </span>
                                <p>
                                    {{ $settings['phone'] }}
                                </p>
                            </div>

                            <img
                                src="{{ asset('site/assets/images/footer/call.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>

                        <a href="mailto:{{ $settings['contact_email'] }}" target="_parent" class="contact-data">
                            <div class="data">
                                <span>
                                {{ __('lang.contact_us') }}
                                </span>

                                <p>
                                    {{ $settings['contact_email'] }}
                                </p>
                            </div>

                            <img
                                src="{{ asset('site/assets/images/footer/sms.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>
                    </div>
                </div>
            </div>

            <div class="copyrights">
                <p>
                    {{ __('lang.rights') }}
                </p>
            </div>
        </div>
    </footer>

    <div class="fixed-icon">
        <a href="tel:{{ $settings['phone'] }}" target="_blank" class="icon primary-color">
            <img
                src="{{ asset('site/assets/images/footer/call.svg') }}"
                loading="lazy"
                alt=""/>
        </a>

        <a href="https://wa.me/{{  $settings['whatsapp_phone']}}" target="_blank" class="icon secondary-color">
            <img
                src="{{ asset('site/assets/images/footer/whats.svg') }}"
                loading="lazy"
                alt=""/>
        </a>
    </div>

@endsection


@section('custom_scripts')

    <script>
        $(document).ready(function () {
            $('#submitBtn').click(function (e) {
                e.preventDefault();
                var formData = $('#service_form').serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{!! route("home.store_order")!!}',
                    data: formData,
                    success: function (response) {
                        $("#DoneModal").modal('show')
                        $("#validation-errors").hide().css("display", "none");
                        $('#service_form')[0].reset();
                    },
                    error: function (xhr) {
                        $('#validation-errors').html('');
                        $.each(xhr.responseJSON.errors, function(key,value) {
                            $('#validation-errors').append('<div class="alert alert-danger">'+value+'</div');
                        });
                    },
                });
            });
        });
    </script>

@endsection
