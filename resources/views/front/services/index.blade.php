@extends('layouts.app')
@section('content')

    <nav class="navbar">
        <div class="container">
            <div class="contain">
                <div class="hamburger">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>

                <a href="/" class="brand-name">
                    <img
                        src="{{ asset('site/assets/images/logo/logo.svg') }}"
                        loading="lazy"
                        alt=""/>
                </a>

                <div class="nav-contain">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="/" class="nav-link {{ request()->is('home') ? 'active' : '' }}">
                                {{ __('lang.home') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="/#about_us" class="nav-link" {{ str_contains(url()->current(), 'about_us') ? 'active' : '' }}>
                                {{ __('lang.about') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="/#why_teamwork" class="nav-link" {{ str_contains(url()->current(), 'why_teamwork') ? 'active' : '' }}>
                                {{ __('lang.why_team')}}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('home.list_services') }}" class="nav-link" {{ str_contains(url()->current(), 'services') ? 'active' : '' }}>
                                {{ __('lang.services')}}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="/#sectors" class="nav-link" {{ str_contains(url()->current(), 'sectors') ? 'active' : '' }}>
                                {{ __('lang.sections') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="/#partners" class="nav-link" {{ str_contains(url()->current(), 'partners') ? 'active' : '' }}>
                                {{ __('lang.partners') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('home.list_professions') }}" class="nav-link {{ str_contains(url()->current(), 'professions') ? 'active' : '' }}">
                                {{ __('lang.professions') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('home.show_contact') }}" class="nav-link {{ str_contains(url()->current(), 'contact_us') ? 'active' : '' }}">
                                {{ __('lang.contact_us') }}
                            </a>
                        </li>
                    </ul>

                    <div class="button-contain">
                        <a href="#" class="lang">
                <span>
                   @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode != LaravelLocalization::getCurrentLocale())
                            <a style="color: #fff" rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                        </a>
                        @endif
                    @endforeach
                </span>

                            <img
                                src="{{ asset('site/assets/images/icons/global.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>

                        <a href="#" class="custom-btn secondary-btn">
                <span>
                  {{ __('lang.request_service') }}
                </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>


    <main>
        <section class="sub-header large-sub-header">
            <div class="container">
                <div class="header-contain">
                    <h1>
                        {{ __('lang.teamwork_services') }}
                    </h1>

                    <p>
                        {{ __('lang.teamwork_services_desc') }}
                    </p>
                </div>
            </div>
        </section>

        <section class="services general-section">
            <div class="container">
                <div class="row">
                    @foreach($services as $service)
                        <div class="col-lg-4 col-12 mb-3">
                            <div class="service-box">
                                <img
                                    src="{{ $service->image() }}"
                                    loading="lazy"
                                    class="service-img"
                                    alt=""/>

                                <div class="contain">
                                    <h2>
                                        {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $service->title_ar : $service->title_en }}
                                    </h2>

                                    <p>
                                        {!!   LaravelLocalization::getCurrentLocale() == 'ar' ? $service->meta_description_ar : $service->meta_description_en !!}
                                    </p>

                                    <a href="{{ route('home.show_service',$service->id) }}" class="custom-btn secondary-btn">
                    <span>
                      {{ __('lang.service_details') }}
                    </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section class="join-us">
            <div class="container">
                <div class="contain">
                    <p>
                        {{ __('lang.join_us') }}
                    </p>

                    <h1>
                        {{ __('lang.join_us_desc') }}
                    </h1>

                    <a href="{{ route('home.list_professions') }}" class="custom-btn primay-btn">
                        <span>
                        {{ __('lang.join_us_now') }}
                        </span>
                    </a>
                </div>
            </div>
        </section>
    </main>


    <div class="modal fade" id="DoneModal" tabindex="-1" role="dialog" aria-labelledby="DoneModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="contain">
                        <img src="{{ asset('site/assets/images/icons/done.svg') }}" loading="lazy" alt=""/>
                        <!-- <img src="assets/images/icons/error.svg" loading="lazy" alt="" /> -->
                        <h1>
                            {{ __('lang.send_success') }}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12 mb-3">
                    <div class="contain">
                        <a href="/" class="brand-name">
                            <img
                                src="{{ asset('site/assets/images/logo/logo.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>

                        <p>
                            {{ LaravelLocalization::getCurrentLocale() == 'ar' ? $settings['website_bio'] : $settings['website_bio_en']}}
                        </p>

                        <ul class="socail">
                            <li>
                                <a href="{{ $settings['facebook_link'] }}" target="_blank">
                                    <img
                                        src="{{ asset('site/assets/images/footer/facebook.svg') }}"
                                        loading="lazy"
                                        alt=""/>
                                </a>
                            </li>

                            <li>
                                <a href="{{ $settings['instagram_link'] }}" target="_blank">
                                    <img
                                        src="{{ asset('site/assets/images/footer/instagram.svg') }}"
                                        loading="lazy"
                                        alt=""/>
                                </a>
                            </li>

                            <li>
                                <a href="{{ $settings['twitter_link'] }}" target="_blank">
                                    <img
                                        src="{{ asset('site/assets/images/footer/x.svg') }}"
                                        loading="lazy"
                                        alt=""/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mb-3">
                    <div class="contain padding-data">
                        <h1>
                            {{ __('lang.site_sections') }}
                        </h1>

                        <ul class="links">
                            <li>
                                <a href="/">
                                    {{ __('lang.home') }}
                                </a>
                            </li>

                            <li>
                                <a href="/#about_us">
                                    {{  __('lang.about') }}
                                </a>
                            </li>

                            <li>
                                <a href="/#why_teamwork">
                                    {{  __('lang.why_team') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('home.list_services') }}">
                                    {{ __('lang.services') }}
                                </a>
                            </li>

                            <li>
                                <a href="/#sectors">
                                    {{ __('lang.sections') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('home.list_professions') }}">
                                    {{ __('lang.professions') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mb-3">
                    <div class="contain">
                        <h1>
                            {{ __('lang.contact_us') }}
                        </h1>

                        <a href="tel:{{ $settings['phone'] }}" target="_parent" class="contact-data">
                            <div class="data">
                                <span>
                                {{ __('lang.contact_us_now') }}
                                </span>
                                <p>
                                    {{ $settings['phone'] }}
                                </p>
                            </div>

                            <img
                                src="{{ asset('site/assets/images/footer/call.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>

                        <a href="mailto:Tw@gmail.com" target="_parent" class="contact-data">
                            <div class="data">
                                <span>
                                {{ __('lang.contact_us') }}
                                </span>

                                <p>
                                    {{ $settings['contact_email'] }}
                                </p>
                            </div>

                            <img
                                src="{{ asset('site/assets/images/footer/sms.svg') }}"
                                loading="lazy"
                                alt=""/>
                        </a>
                    </div>
                </div>
            </div>

            <div class="copyrights">
                <p>
                    {{ __('lang.rights') }}
                </p>
            </div>
        </div>
    </footer>

    <div class="fixed-icon">
        <a href="tel:{{ $settings['phone'] }}" target="_blank" class="icon primary-color">
            <img
                src="{{ asset('site/assets/images/footer/call.svg') }}"
                loading="lazy"
                alt=""/>
        </a>

        <a href="https://wa.me/{{  $settings['whatsapp_phone']}}" target="_blank" class="icon secondary-color">
            <img
                src="{{ asset('site/assets/images/footer/whats.svg') }}"
                loading="lazy"
                alt=""/>
        </a>
    </div>

@endsection
