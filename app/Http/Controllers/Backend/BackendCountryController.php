<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Country;
use Illuminate\Http\Request;

class BackendCountryController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:countries-create', ['only' => ['create','store']]);
//        $this->middleware('can:countries-read',   ['only' => ['show', 'index']]);
//        $this->middleware('can:countries-update',   ['only' => ['edit','update']]);
//        $this->middleware('can:countries-delete',   ['only' => ['delete']]);
    }

    public function index(Request $request)
    {
        $countries = Country::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
            if ($request->q != null)
                $q->where('name_ar', 'LIKE', '%' . $request->q . '%')->orWhere('name_en', 'LIKE', '%' . $request->q . '%');
        })->withCount(['cities'])->orderBy('id', 'DESC')->paginate();

        return view('admin.countries.index', compact('countries'));
    }

    public function create()
    {
        return view('admin.countries.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name_ar' => "required|unique:countries,name_ar|max:190",
            'name_en' => "required|unique:countries,name_en|max:190",
        ]);

        Country::create($data);

        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.countries.index');
    }

    public function show(Country $country)
    {
    }


    public function edit(Country $country)
    {
        return view('admin.countries.edit', compact('country'));
    }


    public function update(Request $request, Country $country)
    {
        $data = $request->validate([
            'name_ar' => "required|max:190|unique:countries,name_ar," . $country->id,
            'name_en' => "required|max:190|unique:countries,name_en," . $country->id,
        ]);
        Country::update($data);
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.countries.index');
    }


    public function destroy(Country $country)
    {
        $country->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.countries.index');
    }
}
