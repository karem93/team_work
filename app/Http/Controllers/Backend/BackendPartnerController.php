<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\MainHelper;
use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;

class BackendPartnerController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:partners-create', ['only' => ['create', 'store']]);
//        $this->middleware('can:partners-read', ['only' => ['show', 'index']]);
//        $this->middleware('can:partners-update', ['only' => ['edit', 'update', 'change_status']]);
//        $this->middleware('can:partners-delete', ['only' => ['delete']]);
    }


    public function index(Request $request)
    {
        $partners = Partner::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.partners.index', compact('partners'));
    }

    public function create()
    {
        return view('admin.partners.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => "nullable|max:190",
        ]);
        $partner = partner::create($data);
        if ($request->hasFile('image')) {
            $image = $partner->addMedia($request->image)->toMediaCollection('image');
            $partner->update(['image' => $image->id . '/' . $image->file_name]);
        }
        MainHelper::move_media_to_model_by_id($request->temp_file_selector, $partner, "description");
        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }


    public function show(Partner $partner)
    {
    }


    public function edit(Partner $partner)
    {
        return view('admin.partners.edit', compact('partner'));
    }

    public function update(Request $request, Partner $partner)
    {
        $data = $request->validate([
            'title' => "nullable|max:190",
        ]);
        $partner->update($data);
        if ($request->hasFile('image')) {
            $image = $partner->addMedia($request->image)->toMediaCollection('image');
            $partner->update(['image' => $image->id . '/' . $image->file_name]);
        }
        MainHelper::move_media_to_model_by_id($request->temp_file_selector, $partner, "description");
        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }

    public function destroy(Partner $partner)
    {
        $partner->delete();
        toastr()->success(__('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }

    public function change_status(Request $request)
    {
        $partner = partner::where('id', $request->id)->firstOrFail();
        $partner->update(['is_active' => !$partner->is_active]);
        return 1;
    }
}
