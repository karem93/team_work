<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\MainHelper;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Profession;
use App\Models\Service;
use Illuminate\Http\Request;

class BackendProfessionController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:professions-create', ['only' => ['create', 'store']]);
//        $this->middleware('can:professions-read', ['only' => ['show', 'index']]);
//        $this->middleware('can:professions-update', ['only' => ['edit', 'update', 'change_status']]);
//        $this->middleware('can:professions-delete', ['only' => ['delete']]);
    }


    public function index(Request $request)
    {
        $professions = Profession::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.professions.index', compact('professions'));
    }

    public function create()
    {
        $countries = Country::all();
        $services = Service::all();
        return view('admin.professions.create',compact('countries','services'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title_ar' => "nullable|max:190|unique:professions,title_ar",
            'title_en' => "nullable|max:190|unique:professions,title_ar",
            'city_id' => "required|exists:cities,id",
            'meta_description_ar' => "nullable|max:10000",
            'meta_description_en' => "nullable|max:10000",
            'description_ar' => "nullable|max:10000",
            'description_en' => "nullable|max:10000",
            'service_id' => "nullable|exists:services,id",
            'shift' => "required",
        ]);
        profession::create($data);
        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.professions.index');
    }


    public function show(Profession $profession)
    {
    }


    public function edit(Profession $profession)
    {
        $countries = Country::all();
        $cities = City::all();
        $services = Service::all();
        return view('admin.professions.edit', compact('profession','countries','cities','services'));
    }

    public function update(Request $request, Profession $profession)
    {
        $data = $request->validate([
            'title_ar' => "nullable|max:190|unique:professions,title_ar,{$profession->id}",
            'title_en' => "nullable|max:190|unique:professions,title_ar,{$profession->id}",
            'city_id' => "required|exists:cities,id",
            'meta_description_ar' => "nullable|max:10000",
            'meta_description_en' => "nullable|max:10000",
            'description_ar' => "nullable|max:10000",
            'description_en' => "nullable|max:10000",
            'service_id' => "nullable|exists:services,id",
            'shift' => "required",
        ]);
        $profession->update($data);
        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.professions.index');
    }

    public function destroy(Profession $profession)
    {
        $profession->delete();
        toastr()->success(__('utils/toastr.successful_process_message'));
        return redirect()->route('admin.professions.index');
    }

}
