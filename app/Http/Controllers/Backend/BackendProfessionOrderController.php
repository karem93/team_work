<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\MainHelper;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\ProfessionOrder;
use App\Models\Service;
use Illuminate\Http\Request;

class BackendProfessionOrderController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:orders-create', ['only' => ['create', 'store']]);
//        $this->middleware('can:orders-read', ['only' => ['show', 'index']]);
//        $this->middleware('can:orders-update', ['only' => ['edit', 'update','change_status']]);
//        $this->middleware('can:orders-delete', ['only' => ['delete']]);
    }


    public function index(Request $request)
    {
        $profession_orders = ProfessionOrder::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.professionOrders.index', compact('profession_orders'));
    }

    public function create()
    {
        return view('admin.orders.create');
    }

    public function store(Request $request)
    {
//        $data = $request->validate([
//            'title' => "required|max:190",
//        ]);
//        $service = Order::create($data);
//        if ($request->hasFile('image')) {
//            $image = $service->addMedia($request->image)->toMediaCollection('image');
//            $service->update(['image' => $image->id . '/' . $image->file_name]);
//        }
//        MainHelper::move_media_to_model_by_id($request->temp_file_selector, $service, "description");
//        toastr()->success(__('utils/toastr.service_store_success_message'), __('utils/toastr.successful_process_message'));
//        return redirect()->route('admin.orders.index');
    }


    public function show(ProfessionOrder $professionOrder)
    {
    }


    public function edit(ProfessionOrder $professionOrder)
    {
//        return view('admin.orders.edit', compact('order'));
    }

    public function update(Request $request, ProfessionOrder $professionOrder)
    {

    }

    public function destroy(ProfessionOrder $professionOrder)
    {
        $professionOrder->delete();
        toastr()->success(__('utils/toastr.service_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.orders.index');
    }

    public function change_status(Request $request)
    {
        $service = Order::where('id', $request->id)->firstOrFail();
        $service->update(['is_active' => !$service->is_active]);
        return 1;
    }
}
