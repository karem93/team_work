<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\MainHelper;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\ServiceTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class BackendServiceController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:services-create', ['only' => ['create', 'store']]);
//        $this->middleware('can:services-read', ['only' => ['show', 'index']]);
//        $this->middleware('can:services-update', ['only' => ['edit', 'update', 'change_status']]);
//        $this->middleware('can:services-delete', ['only' => ['delete']]);
    }


    public function index(Request $request)
    {
        $services = Service::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
            if ($request->q != null)
                $q->where('title_ar', 'LIKE', '%' . $request->q . '%')->orWhere('title_en', 'LIKE', '%' . $request->q . '%');
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        return view('admin.services.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title_ar' => "required|max:190|unique:services,title_ar",
            'title_en' => "required|max:190|unique:services,title_en",
            'meta_description_ar' => "required|max:1000",
            'meta_description_en' => "required|max:1000",
            'description_ar' => "required|string",
            'description_en' => "required|string",
        ]);
        $service = Service::create($data);

        if ($request->hasFile('image')) {
            $image = $service->addMedia($request->image)->toMediaCollection('image');
            $service->update(['image' => $image->id . '/' . $image->file_name]);
        }
        MainHelper::move_media_to_model_by_id($request->temp_file_selector, $service, "description");

        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.services.index');
    }


    public function show(Service $service)
    {
    }


    public function edit(Service $service)
    {
        $tags = Tag::orderBy('id', 'DESC')->get();
        return view('admin.services.edit', compact('service', 'tags'));
    }

    public function update(Request $request, Service $service)
    {
        $data = $request->validate([
            'title_ar' => "required|max:190|unique:services,title_ar,{$service->id}",
            'title_en' => "required|max:190|unique:services,title_en,{$service->id}",
            'meta_description_ar' => "required|max:1000",
            'meta_description_en' => "required|max:1000",
            'description_ar' => "required|string",
            'description_en' => "required|string",
        ]);
        $service->update($data);

        if ($request->hasFile('image')) {
            $image = $service->addMedia($request->image)->toMediaCollection('image');
            $service->update(['image' => $image->id . '/' . $image->file_name]);
        }
        MainHelper::move_media_to_model_by_id($request->temp_file_selector, $service, "description");

        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.services.index');
    }

    public function destroy(Service $service)
    {
        $service->delete();
        toastr()->success( __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.services.index');
    }

    public function change_status(Request $request)
    {
        $service = Service::where('id', $request->id)->firstOrFail();
        $service->update(['is_active' => !$service->is_active]);
        return 1;
    }
}
