<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;

class BackendCityController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:cities-create', ['only' => ['create','store']]);
//        $this->middleware('can:cities-read',   ['only' => ['show', 'index']]);
//        $this->middleware('can:cities-update',   ['only' => ['edit','update']]);
//        $this->middleware('can:cities-delete',   ['only' => ['delete']]);
    }

    public function index(Request $request)
    {
        $cities = City::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
            if ($request->q != null)
                $q->where('name_ar', 'LIKE', '%' . $request->q . '%')->orWhere('name_en', 'LIKE', '%' . $request->q . '%');
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.cities.index', compact('cities'));
    }

    public function create()
    {
        $countries = Country::all();
        return view('admin.cities.create',compact('countries'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name_ar' => "required|unique:cities,name_ar|max:190",
            'name_en' => "required|unique:cities,name_en|max:190",
            'country_id' => "required|exists:countries,id",
        ]);

        City::create($data);

        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.cities.index');
    }

    public function show(City $city)
    {
    }


    public function edit(City $city)
    {
        $countries = Country::all();
        return view('admin.cities.edit', compact('city','countries'));
    }


    public function update(Request $request, City $city)
    {
        $data = $request->validate([
            'name_ar' => "required|max:190|unique:cities,name_ar," . $city->id,
            'name_en' => "required|max:190|unique:cities,name_en," . $city->id,
            'country_id' => "required|exists:countries,id",
        ]);
        City::update($data);
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.cities.index');
    }


    public function destroy(City $city)
    {
        $city->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.cities.index');
    }

    public function fetchCity(Request $request)
    {
        $data['cities'] = City::where("country_id", $request->country_id)
            ->get(["name_ar", "id"]);
        return response()->json($data);
    }
}
