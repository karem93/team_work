<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\MainHelper;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Service;
use Illuminate\Http\Request;

class BackendOrderController extends Controller
{

    public function __construct()
    {
//        $this->middleware('can:orders-create', ['only' => ['create', 'store']]);
//        $this->middleware('can:orders-read', ['only' => ['show', 'index']]);
//        $this->middleware('can:orders-update', ['only' => ['edit', 'update','change_status']]);
//        $this->middleware('can:orders-delete', ['only' => ['delete']]);
    }


    public function index(Request $request)
    {
        $orders = Order::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.orders.index', compact('orders'));
    }

    public function create()
    {
        return view('admin.orders.create');
    }

    public function store(Request $request)
    {
       //
    }


    public function show(Order $order)
    {
        //
    }


    public function edit(Order $order)
    {
//        return view('admin.orders.edit', compact('order'));
    }

    public function update(Request $request, Service $service)
    {

    }

    public function destroy(Order $order)
    {
        $order->delete();
        toastr()->success(__('utils/toastr.service_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.orders.index');
    }

    public function change_status(Request $request)
    {
        $service = Order::where('id', $request->id)->firstOrFail();
        $service->update(['is_active' => !$service->is_active]);
        return 1;
    }
}
