<?php

namespace App\Http\Controllers;

use App\Helpers\MainHelper;
use App\Models\Category;
use App\Models\City;
use App\Models\Order;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Profession;
use App\Models\ProfessionOrder;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Http\Request;

class FrontController extends Controller
{

    public function index(Request $request)
    {
        $settings = Setting::pluck('value', 'key')->all();
        $message_page = Page::where('slug', 'message')->first();
        $vision_page = Page::where('slug', 'vision')->first();
        $why_us_page = Page::where('slug', 'whyus')->first();
        $services = Service::all();
        $categories = Category::all();
        $partners = Partner::all();
        return view('front.index', compact('settings', 'services', 'categories', 'partners', 'message_page', 'vision_page', 'why_us_page'));
    }

    public function listServices()
    {
        $services = Service::all();
        return view('front.services.index', compact('services'));
    }

    public function showService($id)
    {
        $service = Service::find($id);
        return view('front.services.single', compact('service'));
    }


    public function listProfessions()
    {
        $professions = Profession::paginate(10);
        return view('front.professions.index', compact('professions'));
    }

    public function showProfession($id)
    {
        $profession = Profession::find($id);
        $cities = City::all();
        return view('front.professions.single', compact('profession','cities'));
    }

    public function contactUs()
    {
        $services = Service::all();
        return view('front.contact',compact('services'));
    }

    public function storeOrder(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email',
            'phone' => 'required|numeric|digits_between:8,11',
            'message' => 'required|min:3|max:255',
            'service_id' => 'required'
        ]);

        $service_order = Order::create($data);
        return response()->json(['message' => 'Data stored successfully', 'data' => $service_order]);
    }



    public function storeProfessionOrder(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email',
            'phone' => 'required|numeric|digits_between:8,11',
            'skills' => 'required|string|min:3|max:255',
            'profession_id' => 'required',
            'city_id' => 'required|exists:cities,id',
            'district' => 'required|string|min:3|max:255',
            'cv' => 'required|mimes:pdf,doc,docx|max:2048',
        ]);

        $service_order = ProfessionOrder::create($data);

        if ($request->hasFile('cv')) {
            $service_order->addMedia($request->cv)->toMediaCollection('cv');
        }
        MainHelper::move_media_to_model_by_id($request->temp_file_selector, $service_order, "description");

        return response()->json(['message' => 'Data stored successfully', 'data' => $service_order]);
    }

}

