$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 700) {
            $(".scroll-top-button").fadeIn();
        } else {
            $(".scroll-top-button").fadeOut();
        }
    });

    $(function () {
        $(document).scroll(function () {
            var $nav = $(".navbar");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        });
    });

    $(".scroll-top-button").on('click', function () {
        $('html , body').animate({
            scrollTop: 0
        }, 100);
    });

    $('.hamburger').click(function () {
        $('.hamburger').toggleClass('active');
        $('.nav-contain').toggleClass('active-nav');
        $('body').toggleClass('overflowNone')
    });

    $('.nav-contain .nav-link').click(function () {
        $('.hamburger').removeClass('active');
        $('.nav-contain').removeClass('active-nav');
        $('body').removeClass('overflowNone')
    });

    var swiper = new Swiper(".swiper-header", {
        loop: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });

    var swiper = new Swiper(".swiper-partners", {
        spaceBetween: 10,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        breakpoints: {
            300: {
                slidesPerView: 2,
                spaceBetween: 18,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 18,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 18,
            },
            1024: {
                slidesPerView: 6,
                spaceBetween: 18,
            },
        },
    });

    var swiper = new Swiper(".swiper-services", {
        spaceBetween: 10,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            300: {
                slidesPerView: 1,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 18,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 18,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 18,
            },
        },
    });

    $("[data-fancybox]").fancybox({
        selector: '[data-fancybox="images"]',
        loop: true
    });

    $('.file__input--file_worker').on('change', function (event) {
        var files = event.target.files;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            $("<div class='file__value'><div class='file__value--text'>" + file.name + "</div><div class='file__value--remove' data-id='" + file.name + "' ></div></div>").insertAfter('#file__input_worker');
        }
    });

    // Remove file value on click
    $('body').on('click', '.file__value--remove', function () {
        $(this).parent().remove();
        // Also clear the corresponding file input
        var fileName = $(this).data('id');
        $('.file__input--file_worker').each(function () {
            if (this.files.length > 0 && this.files[0].name === fileName) {
                $(this).val(''); // Clear the file input value
            }
        });
    });
});

